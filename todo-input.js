
import React from 'react';

var TodoInput = React.createClass({
    getInitialState() {
        return {
            todo: {
                description: '',
                done: false
            }
        }
    },
    handleInput(evt) {
        // console.log(evt.target);
        // console.log(evt.target.value);
        this.setState({
            description: evt.target.value
        });
        //var evt = e.nativeEvent;
        //console.log('handleClick !', evt, evt.keyCode || event.which);
    },
    handleClick() {
        this.props.handleAddTodo(this.state.todo);
        // this.setState({ todo: {description: '', done: false} });
    },
    render () {
        return (
            <div>
                <input onChange={this.handleInput} placeholder="hahahah" value={this.state.description} />
                <button onClick={this.handleClick}>Add Todo</button>
            </div>
        );
    }
});

module.exports = TodoInput;
