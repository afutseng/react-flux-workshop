
import React from 'react';

var TodoItem = React.createClass({

    render() {
        return (
            <div className={this.props.item.done ? 'done' : ''}> {this.props.item.description} </div>
        );
    }
});

module.exports = TodoItem;
