
import React from 'react';
import TodoItem from './todo-item.js';

var TodoList = React.createClass({

    render () {
        console.log('props todos', this.props.todos)
        return (
            <div>
            { this.props.todos.map((item, i) => <TodoItem key={i} item={item} />) }
            </div>
        );
    }
});

module.exports = TodoList;
