import React from 'react';
import TodoList from './todo-list.js';
import TodoInput from './todo-input.js';


var App = React.createClass({
    getInitialState() {
        return {todos: [
            {description: 'apple', done: false},
            {description: 'banana', done: false}
        ]};
    },
    handleAddTodo(todo) {
        //console.log('handleAddTodo = ', todo);
        //console.log( 'todos = ', todos );
        this.setState({
            todos: this.state.todos.concat(todo)
        });
        console.log( 'todos after = ', this.state.todos );
    },
    render () {
        return (
            <div>
                <h3> In React app. </h3>
                <TodoList todos={this.state.todos} />
                <TodoInput handleAddTodo={ this.handleAddTodo } />
            </div>
        );
    }
});

module.exports = App;